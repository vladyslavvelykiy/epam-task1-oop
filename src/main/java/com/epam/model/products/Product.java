package com.epam.model.products;

public class Product {
    private double calories;
   // private boolean expirationDate; //розширити до типу Дата
    private int qualityOfGoods;
    private boolean qualityСontrol; // вищий - 0 1 //
    private double pricePerKilogram;

    public Product(double calories,  int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        this.calories = calories;
        this.qualityOfGoods = qualityOfGoods;
        this.qualityСontrol = qualityСontrol;
        this.pricePerKilogram = pricePerKilogram;
    }

    @Override
    public String toString() {
        return "Product{" +
                "calories=" + calories +
                ", qualityOfGoods=" + qualityOfGoods +
                ", qualityСontrol=" + qualityСontrol +
                ", pricePerKilogram=" + pricePerKilogram +
                '}';
    }
}
