package com.epam.model.products;

public class Flour extends Product{
    public Flour(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Flour " + super.toString();
    }
}
