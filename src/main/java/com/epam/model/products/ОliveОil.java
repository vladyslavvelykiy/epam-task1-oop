package com.epam.model.products;

public class ОliveОil extends Product {
    public ОliveОil(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Оlive oil " + super.toString();
    }
}
