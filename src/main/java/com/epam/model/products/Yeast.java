package com.epam.model.products;

//Дріжджі
public class Yeast extends Product{
    public Yeast(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Yeast " + super.toString();
    }
}
