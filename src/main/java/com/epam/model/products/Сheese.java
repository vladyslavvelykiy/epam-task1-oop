package com.epam.model.products;

public class Сheese extends Product{
    public Сheese(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Сheese " + super.toString();
    }
}
