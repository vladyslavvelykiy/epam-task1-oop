package com.epam.model.products;

public class Tomato extends Product{
    public Tomato(double calories,  int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories,  qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Tomato " + super.toString();
    }
}
