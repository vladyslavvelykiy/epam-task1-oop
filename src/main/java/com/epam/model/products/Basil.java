package com.epam.model.products;

//Базилік
public class Basil extends Product {
    public Basil(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Basil " + super.toString();
    }
}
