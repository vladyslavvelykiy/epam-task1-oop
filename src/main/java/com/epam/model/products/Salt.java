package com.epam.model.products;

public class Salt extends Product {
    public Salt(double calories, int qualityOfGoods, boolean qualityСontrol, double pricePerKilogram) {
        super(calories, qualityOfGoods, qualityСontrol, pricePerKilogram);
    }

    @Override
    public String toString() {
        return "Salt " + super.toString();
    }
}
