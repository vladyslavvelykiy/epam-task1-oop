package com.epam.model.products.enums;

public enum Products {
    Basil, Flour, Salt, Tomato, Water, Yeast, OliveOil, Cheese;
}
