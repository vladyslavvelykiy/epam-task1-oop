package com.epam.model;

public class BakedPizza {
    private String name;
    private double price;
    private double weight;
    private double size;

    public BakedPizza(String name, double price, double weight, double size) {
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.size = size;
    }
}
