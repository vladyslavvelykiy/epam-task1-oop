package com.epam.model;

import com.epam.model.products.Product;
import com.epam.model.products.enums.Products;

import java.util.ArrayList;
import java.util.List;

import static com.epam.model.products.enums.Products.Basil;
import static com.epam.model.products.enums.Products.Flour;

public class Pizza implements PizzaModel {
    BakedPizza bakedPizza;
    List<Product> products;

    public Pizza() {
        products = new ArrayList<Product>();
    }

    public boolean addToPizza(Product product){
        return products.add(product);
    }
    public boolean removeFromPizza(Product product){
        return products.remove(product);
    }


    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "bakedPizza=" + bakedPizza +
                ", products=" + products +
                '}';
    }

    @Override
    public void makePizza() {
        if(products.contains(Products.values())){
        bakedPizza = new BakedPizza("Margarita", 70, 600, 40);
        }
    }

    @Override
    public void getStat() {

    }
}
