package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class RecipeMenu extends Menu{
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;
    private static Scanner input = new Scanner(System.in);

    public RecipeMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Add recipe");
        menu.put("2", "2 - Show recipies");
        menu.put("3", "3 - Delete recipe");
        menu.put("Q", "Q - Exit");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::addRecipe);
        menuMethod.put("2", this::showRecipe);
        menuMethod.put("3", this::deleteRecipe);

    }
    public void addRecipe(){
        System.out.println("add recipe");
    }
    public void showRecipe(){
        System.out.println("show recipe");
    }
    public void deleteRecipe(){
        System.out.println("delete recipe");
    }

    @Override
    public void show() {
        String keyMenu;
        do {

            outputMenu();

            keyMenu = input.nextLine().toUpperCase();
            try {
                menuMethod.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
//    }
    }

    @Override
    public void outputMenu() {
        System.out.println("---------------RECIPE---MENU---------------");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("-----------------------------------------");
    }
}
