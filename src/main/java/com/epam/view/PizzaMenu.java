package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class PizzaMenu extends Menu {
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;
    private static Scanner input = new Scanner(System.in);


    public PizzaMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Make pizza");
        menu.put("2", "2 - Show all pizza");
        menu.put("Q", "Q - Exit");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::makePizza);
        menuMethod.put("2", this::printPizza);
    }

    //Methods for controller
    public void makePizza() {
        System.out.println("Making pizza");
    }

    public void printPizza() {
        System.out.println("printing pizza");
    }
    //***********************

    @Override
    public void outputMenu() {
        System.out.println("---------------PIZZA---MENU---------------");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("-----------------------------------------");
    }

    @Override
    public void show() {
        String keyMenu;
        do {

            outputMenu();

            keyMenu = input.nextLine().toUpperCase();
            try {
                menuMethod.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


}
