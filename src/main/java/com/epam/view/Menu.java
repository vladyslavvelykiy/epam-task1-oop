package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class Menu {

    public Menu() {
    }

    public abstract void show();
    public abstract void outputMenu();

}
