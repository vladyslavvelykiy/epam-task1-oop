package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    PizzaMenu pizzaMenu;
    RecipeMenu recipeMenu;
    ProductMenu productMenu;
    Map<String, String> menu;
    Map<String, Printable> methodMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {

        pizzaMenu = new PizzaMenu();
        recipeMenu = new RecipeMenu();
        productMenu = new ProductMenu();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print products menu");
        menu.put("2", "2 - Print Pizza menu");
        menu.put("3", "3 - Print recipe of pizza");
        menu.put("q", "Exit");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::printPruductMenu);
        methodMenu.put("2", this::printPizzaMenu);
        methodMenu.put("3", this::printRecipeMenu);

    }

    public void printPruductMenu(){
        productMenu.show();
    }
    public void printPizzaMenu() {
        pizzaMenu.show();
    }

    public void printRecipeMenu() {
        recipeMenu.show();
    }


    public void outputMainMenu() {
        System.out.println("---------------MAIN---MENU---------------");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("-----------------------------------------");
    }

    public void show() {
        String keyMenu;
        do {

            outputMainMenu();

            keyMenu = input.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
