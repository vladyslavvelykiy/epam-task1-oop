package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ProductMenu extends Menu{
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;
    private static Scanner input = new Scanner(System.in);

    public ProductMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Add product");
        menu.put("2", "2 - Output list of products");
        menu.put("3", "3 - Delete product");
        menu.put("q", "Q - Exit");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::addProduct);
        menuMethod.put("2", this::printProducts);
        menuMethod.put("3", this::deleteProduct);
    }
    public void addProduct(){
        System.out.println("add products");
    }
    public void printProducts(){
        System.out.println("print products");
    }
    public void deleteProduct(){
        System.out.println("delete product");
    }

    @Override
    public void outputMenu() {
        System.out.println("---------------PRODUCTS---MENU---------------");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("-----------------------------------------");
    }

    @Override
    public void show() {
        String keyMenu;
        do {

            outputMenu();

            keyMenu = input.nextLine().toUpperCase();
            try {
                menuMethod.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


}
